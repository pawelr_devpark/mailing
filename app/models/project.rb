class Project < ActiveRecord::Base

  has_many :emails

  scope :sortByName, lambda{order("project_name ASC")}

  # def to_s
  #   "Name:#{self.email_address}"
  # end

end

class Email < ActiveRecord::Base

  belongs_to :project

  scope :sortByEmailAddress, lambda{order("email_address ASC")}
end

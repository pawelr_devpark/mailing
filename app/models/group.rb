class Group < ActiveRecord::Base
  has_many :users

  scope :sortByName, lambda{order("name ASC")}

end

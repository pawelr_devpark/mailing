class GroupsController < ApplicationController

  # before_action :check_access, :except => [:login, :logout, :access]
  layout 'admin'

  before_action :find_group_by_id

  def index
    @groups = Group.all.sortByName
  end

  def new
    @group = Group.new
  end

  def create
    @group = Group.new(grup_params)
    if @group.save
      flash[:success] = "The group #{@group.name} has been correctly added."
      redirect_to :action => :index
    else
      flash[:error] = "The group #{@group.name} has not been correctly added."
      render :index
    end
  end

  def edit
    @group = Group.find(params[:id])
  end

  def update
    @group = Group.find(params[:id])
    if @group.update_attributes(grup_params)
      flash[:success] = "The group #{@group.name} has been correctly updated."
      redirect_to :action => :index
    else
      flash[:error] = "The group #{@group.name} has not been correctly updated."
      render :new
    end
  end

  # def show
  #   @group = Group.find(params[:id])
  # end


  def delete
    @group.destroy
    redirect_to :action => :index
  end

  protected

    def grup_params
      params.require(:group).permit(:name)
    end

    def find_group_by_id
      if params[:id]
        @group = Group.find(params[:id])
      end
    end

end

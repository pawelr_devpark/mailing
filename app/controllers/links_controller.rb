class LinksController < ApplicationController

  def self.run

    obj = LinksController.new

    local_projects = obj.get_local_projects
    # puts local_projects

    # buzzstream_links_url_data = obj.get_buzzstream_links()

    # local_links = obj.get_local_links()

    # puts local_links

    if local_projects.any?


    #   if !buzzstream_links_url_data["list"].empty?
      # local_projects.inspect
      local_projects.each do |local_project_id|

        buzzstream_links_url_data = obj.get_buzzstream_links_url_by_project_id(local_project_id)
          # print buzzstream_links_url_data.inspect

        if !buzzstream_links_url_data["list"].empty?

          buzzstream_links_url_data["list"].each do |link_url|

            buzzstream_links_ids = link_url.gsub('https://api.buzzstream.com/v1/links/', '')

            # break if buzzstream_links_ids.to_i != 53552706
            # puts buzzstream_links_ids.inspect

                      # link = buzzstream_links_url_data["list"].first
                      # buzzstream_links_ids = link.gsub('https://api.buzzstream.com/v1/links/', '')

            buzzstream_link_details = obj.get_buzzstream_links(buzzstream_links_ids.to_i)
            # puts buzzstream_link_details.inspect

            local_link = obj.get_local_link(buzzstream_links_ids.to_i)

            # puts local_link.inspect

            if local_link.present?

              if local_link.link_flag != buzzstream_link_details['linkingFlag']
                # puts 'rozne flagi'

                project_emails = Email.where("project_id = #{local_project_id}").to_a
                # puts project_emails.inspect

                if !project_emails.empty?

                  project_emails.each do |email|
                    # puts email.email_receiver_name

                    obj.link_flag_changed(local_link.buzzstream_link_id)
                    SenderNotifier.send_nofification(email.email_receiver_name).deliver

                  end

                end #!project_emails.empty?

              # else  #local_link.link_flag != buzzstream_link_details['linkingFlag']

                # puts 'rowne flagi'

              end #local_link.link_flag != buzzstream_link_details['linkingFlag']

            else #local_link.present?

              obj.add_new(buzzstream_links_ids)

            end #local_link.present?

          end #buzzstream_links_url_data["list"].each

        end #if !buzzstream_links_url_data["list"].empty?

      end #local_projects.each

    end #if local_projects.any?

  end

  # private

  def add_new(id = nil)
    project_new = Link.new
    project_new.buzzstream_link_id = id
    project_new.link_flag = false
    project_new.emails_sent = false
    project_new.save
  end

  def link_flag_changed(id = nil)
    obj_link = Link.find_by buzzstream_link_id: id.to_i
    obj_link.update(emails_sent: true, link_flag: true, last_chenged: Time.now)
  end

  def get_local_link(id = nil)
    Link.find_by(:buzzstream_link_id => id)
  end

  def get_local_links
    Link.all.collect {|l| [l.buzzstream_project_id, l.link_flag]}
  end

  def get_buzzstream_links_url_by_project_id(id = nil)
    require 'oauth'
    require 'json'

    # consumer = OAuth::Consumer.new(key, secret, { :site => 'https://api.buzzstream.com' })
    # access_token = OAuth::AccessToken.from_hash(consumer, :oauth_token => key, :oauth_token_secret => secret)

    consumer_key="e7d0471c-7ab0-48ce-976f-4ff722d92dd1"
    consumer_secret="EZhjZhjZJan0X9r6NsoVkMrblnRuAZM1onb6rw8Z3dro3hnaUgBxUSnFVar65NYE"

    #DEvpark
    # consumer_key="ce299cb0-d3a6-4db4-b59d-effbe161424e"
    # consumer_secret="r1yJC1qtLhusG3aTj2KhC3lPwZJmx5sUvuigBi9zNt0xlYWAsxX6R2LABN2LV5S5"

    base_url="https://api.buzzstream.com"

    #Create a consumer for the buzzstream api
    consumer = OAuth::Consumer.new(consumer_key, consumer_secret,
                    :site => base_url,
                    :http_method => :get)

    #From the consumer create an access token for 2leg authentication
    access_token = OAuth::AccessToken.new consumer

    #Perform request
    if id.nil?
      response = access_token.get("/v1/links")
    else
      response = access_token.get("/v1/links?project=#{id}")
    end

    #Print response
    json_response = JSON.parse(response.body)
  end

  def get_buzzstream_links(id = nil)

    require 'oauth'
    require 'json'

    # consumer = OAuth::Consumer.new(key, secret, { :site => 'https://api.buzzstream.com' })
    # access_token = OAuth::AccessToken.from_hash(consumer, :oauth_token => key, :oauth_token_secret => secret)

     consumer_key="e7d0471c-7ab0-48ce-976f-4ff722d92dd1"
    consumer_secret="EZhjZhjZJan0X9r6NsoVkMrblnRuAZM1onb6rw8Z3dro3hnaUgBxUSnFVar65NYE"

    #DEvpark
    # consumer_key="ce299cb0-d3a6-4db4-b59d-effbe161424e"
    # consumer_secret="r1yJC1qtLhusG3aTj2KhC3lPwZJmx5sUvuigBi9zNt0xlYWAsxX6R2LABN2LV5S5"

    base_url="https://api.buzzstream.com"

    #Create a consumer for the buzzstream api
    consumer = OAuth::Consumer.new(consumer_key, consumer_secret,
                    :site => base_url,
                    :http_method => :get)

    #From the consumer create an access token for 2leg authentication
    access_token = OAuth::AccessToken.new consumer

    #Perform request
    if id.nil?
      response = access_token.get("/v1/links")
    else
      response = access_token.get("/v1/links/#{id}")
    end

    #Print response
    json_response = JSON.parse(response.body)
    # for value in @json_response
    #         puts value
    # end
  end

  def get_local_projects
    Project.all.collect {|p| p.buzzstream_project_id}
  end

end

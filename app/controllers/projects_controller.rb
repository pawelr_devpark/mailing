class ProjectsController < ApplicationController

  # layout 'admin'

  layout 'admin'

  # before_action :find_email_by_id

  def index
    @emails = Email.where(:project_id => params[:project_id]).sortByEmailAddress
    # @emails = Email.find_all_by_project_id(params[:project_id])
    @projects = Project.all.sortByName

    # @emails = Email.all.sortByEmailAddress
  end

  def self.run

    obj = ProjectsController.new
    buzzstream_projects = obj.get_buzzstream_projects()

    buzzstream_projects_ids = Array.new
    if !buzzstream_projects["list"].empty?
      buzzstream_projects["list"].each do |link|
        buzzstream_projects_ids << link.gsub('https://api.buzzstream.com/v1/projects/', '')
      end
    end

    local_projects = obj.get_local_projects()

    # Find to add elements
    to_add = Array.new
    if buzzstream_projects_ids.any?
      buzzstream_projects_ids.each do |buzz_id|
        if !local_projects.include?(buzz_id.to_i)
          to_add << buzz_id
        end
      end
    end
    # puts to_add

    # Find to remove elements
    to_remove = Array.new
    if local_projects.any?
      local_projects.each do |local_id|
        if !buzzstream_projects_ids.include?(local_id.to_s)
            to_remove << local_id
        end
      end
    end
    # puts to_remove

    #Add elements
    if to_add.any?
      to_add.each do |buzzstream_project_id|
          buzzstream_project_details = obj.get_buzzstream_projects(buzzstream_project_id)
          buzzstream_project_name = nil
          if buzzstream_project_details.any?
            buzzstream_project_name = buzzstream_project_details["name"]
          end
          obj.add_new(buzzstream_project_id, buzzstream_project_name)
      end
    end

    #Remove elements
    if to_remove.any?
      to_remove.each do |buzzstream_project_id|
          obj.remove_old(buzzstream_project_id)
      end
    end

  end

  # private

  def add_new(id = nil, name = nil)
    project_new = Project.new
    project_new.buzzstream_project_id = id
    project_new.project_name = name
    project_new.flag = true
    project_new.save
  end

  def remove_old(id = nil)
    project = Project.find_by buzzstream_project_id: id
    project.destroy
  end

  def get_local_projects
    Project.all.collect {|p| p.buzzstream_project_id}
  end

  def get_buzzstream_projects(id = nil)

    require 'oauth'
    require 'json'

    # consumer = OAuth::Consumer.new(key, secret, { :site => 'https://api.buzzstream.com' })
    # access_token = OAuth::AccessToken.from_hash(consumer, :oauth_token => key, :oauth_token_secret => secret)

    consumer_key="e7d0471c-7ab0-48ce-976f-4ff722d92dd1"
    consumer_secret="EZhjZhjZJan0X9r6NsoVkMrblnRuAZM1onb6rw8Z3dro3hnaUgBxUSnFVar65NYE"

    #DEVPark
    # consumer_key="ce299cb0-d3a6-4db4-b59d-effbe161424e"
    # consumer_secret="r1yJC1qtLhusG3aTj2KhC3lPwZJmx5sUvuigBi9zNt0xlYWAsxX6R2LABN2LV5S5"

    base_url="https://api.buzzstream.com"

    #Create a consumer for the buzzstream api
    consumer = OAuth::Consumer.new(consumer_key, consumer_secret,
                    :site => base_url,
                    :http_method => :get)

    #From the consumer create an access token for 2leg authentication
    access_token = OAuth::AccessToken.new consumer

    #Perform request
    if id.nil?
      response = access_token.get("/v1/projects")
    else
      response = access_token.get("/v1/projects/#{id}")
    end

    #Print response
    json_response = JSON.parse(response.body)
    # for value in @json_response
    #         puts value
    # end
  end


end

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def check_access
    unless session[:user_id]
      flash[:notice] = "Your are not logged in"
      redirect_to :controller => :admin, :action => :login
      return false
    else
      # @current_user ||= User.find(session[:user_id]) if session[:user_id]
      @current_user =  session[:user_name]
      return true
    end
  end

  private
end

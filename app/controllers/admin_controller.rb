class AdminController < ApplicationController
  layout 'application'
  # layout 'admin'
  # require 'oauth'
  # require 'json'

  before_action :check_access, :except => [:login, :logout, :access]

  def index
  end

  def login

  end

  def access

    if params[:login].present? && params[:password].present?
      search_admin = User.where(:login => params[:login]).first
      if search_admin
        admin_authenticate = search_admin.authenticate(params[:password])
      end
    end

    if admin_authenticate
      session[:user_id] = admin_authenticate.id
      session[:user_name] = admin_authenticate.login
      flash[:success] = "You successfully logged in."
      redirect_to(:action => 'index')
    else
      flash[:error] = "Error: invalid user name or password."
      redirect_to(:action => 'login')
    end

  end


  def logout
    session[:user_id] = nil
    session[:user_name] = nil
    flash[:success] = "You logged out"
    redirect_to(:action => "login")
  end

end


class UsersController < ApplicationController
  layout 'admin'

  def index
    if params[:group_id]
      @group = Group.find(params[:group_id])
      @users = @group.users.all
    else
      @users = User.all
    end
  end

  def new
    @user = User.new
    @groups = Group.all.order("name ASC")
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "The user #{@user.login} has been correctly added."
      redirect_to :action => :index
    else
      flash[:error] = "The user #{@user.login} has not been correctly added."
      render :index
    end
  end

  def edit
    @groups = Group.all.order("name ASC")
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "The user #{@user.login} has been correctly updated."
      redirect_to :action => :index
    else
      flash[:error] = "The user #{@user.login} has not been correctly updated."
      render :new
    end
  end

  def delete
    User.find(params[:id]).destroy
    redirect_to :action => :index
  end

  private
    def user_params
      params.require(:user).permit(:group_id, :login, :name, :surname, :email, :password, :password_confirmation)
    end
end

class EmailsController < ApplicationController

  before_action :check_access, :except => [:login, :logout, :access]

  layout 'admin'

  before_action :find_email_by_id

  def index
    # @projects = Project.all
    @emails = Email.all.sortByEmailAddress
  end

  def new
    @projects = Project.all
    @email = Email.new
  end

  def create
    @email = Email.new(email_params)
    if @email.save
      flash[:success] = "The email #{@email.email_address} has been correctly added."
      redirect_to :action => :index
    else
      flash[:error] = "The email #{@email.email_address} has not been correctly added."
      render :index
    end
  end

  def edit
    @projects = Project.all
    @email = Email.find(params[:id])
  end

  def update
    @email = Email.find(params[:id])
    if @email.update_attributes(email_params)
      flash[:success] = "The email #{@email.email_address} has been correctly updated."
      redirect_to :action => :index
    else
      flash[:error] = "The email #{@email.email_address} has not been correctly updated."
      render :new
    end
  end

  # def show
  #   @email = Email.find(params[:id])
  # end


  def delete
    @email.destroy
    redirect_to :action => :index
  end

  protected

    def email_params
      params.require(:email).permit(:email_address, :email_receiver_name, :project_id)
    end

    def find_email_by_id
      if params[:id]
        @email = Email.find(params[:id])
      end
    end

end

class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.integer :buzzstream_link_id, :null => false
      t.boolean :link_flag, :default => false
      t.datetime :last_chenged
      t.boolean :emails_sent, :default => false
      t.timestamps
    end
  end
end

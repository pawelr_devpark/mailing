class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :email_address, :null => true
      t.string :email_receiver_name, :null => true
      t.references :project
      t.timestamps
    end
  end
end

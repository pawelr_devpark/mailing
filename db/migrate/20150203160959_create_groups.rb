class CreateGroups < ActiveRecord::Migration

  def up
    create_table :groups do |t|
      t.string :name, :limit => 40, :null => false
      t.timestamps
    end
    add_index(:groups, :name)
  end

  def down
    drop_table :groups
  end

end

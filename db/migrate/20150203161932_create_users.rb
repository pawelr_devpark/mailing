class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.integer :group_id, :null => false
      t.string :login, :limit => 20, :null => false
      t.string :name, :limit => 30, :null => true, :default => ''
      t.string :surname, :limit => 50, :null => true, :default => ''
      t.string :email, :limit => 100, :null => false
      t.string :role, :limit => 7, :null => true, :default => 'visitor'
      t.string :password_digest
      t.timestamps
    end
  end

  def down
    drop_table :users
  end
end

class CreateProjects < ActiveRecord::Migration
  def up
    create_table :projects do |t|
      t.integer :buzzstream_project_id
      t.string :project_name

      t.timestamps
    end
  end
  def down
    drop_table :projects
  end
end

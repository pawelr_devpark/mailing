# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150209131055) do

  create_table "emails", force: true do |t|
    t.string   "email_address"
    t.string   "email_receiver_name"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "name",       limit: 40, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups", ["name"], name: "index_groups_on_name"

  create_table "links", force: true do |t|
    t.integer  "buzzstream_link_id",                 null: false
    t.boolean  "link_flag",          default: false
    t.datetime "last_chenged"
    t.boolean  "emails_sent",        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects", force: true do |t|
    t.integer  "buzzstream_project_id"
    t.string   "project_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "flag",                  default: false
  end

  create_table "users", force: true do |t|
    t.integer  "group_id",                                        null: false
    t.string   "login",           limit: 20,                      null: false
    t.string   "name",            limit: 30,  default: ""
    t.string   "surname",         limit: 50,  default: ""
    t.string   "email",           limit: 100,                     null: false
    t.string   "role",            limit: 7,   default: "visitor"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
